#include <iostream>
#include <string>

int main()
{
    std::string str{ "Hello World!" };

    std::cout << "String:\n"<< str << std::endl;
    std::cout << "String length:\t\t" << str.length() << std::endl;
    std::cout << "String first symbol:\t\'" << str.front() << '\'' << std::endl;
    std::cout << "String last symbol:\t\'" << str.back() << '\'' << std::endl;

    return 0;
}
